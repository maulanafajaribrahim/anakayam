<?php declare(strict_types = 1);
    if(isset($_POST["mulai"])){    
        $number = $_POST["number"];
        if($number <= 0 || empty($number)){
            ?>
                <script>
                    if (window.confirm('Masukan Banyak anak ayam')) 
                    {
                        window.location.href='index.php';
                    }else{
                        window.location.href='index.php';
                    }
                </script>
            <?php
            exit;
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anak Ayam</title>
    <style>
        *{
            padding: 0;
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }
        body{
            background-color: rgb(41, 44, 46);
            display: flex;
            flex-direction: column;
            text-align: center;
            width: 100%;
            color: #fff;
        }
        form h1{
            color: aqua;
            padding: 30px 0;
        }
        form{
            width: 500px;
            display: flex;
            margin: 0 auto;
            flex-direction: column;
            padding: 40px 0;
        }
        form button{
            padding: 8px 0;
            background-color: aquamarine;
            border: none;
            cursor: pointer;
            font-size: 24px;
            transition: .5s;
        }
        form button:active{
            scale: .5;
        }
        form label{
            font-size: 24px;
            text-align: left;
            font-weight: 100;
        }
        form input{
            width: 100%;
            padding: 10px 0;    
            border: none;
            margin: 5px 0;
            font-size: 18px;
        }
        form img{
            width: 300px;
            margin: 0 auto;
        }
        .lagu{
            width: 500px;
            border: 1px solid aqua;
            margin: 0 auto;
            padding: 20px 0;
        }
        .lagu p{
            color: aqua;
        }
        .jumlah{
            padding: 10px 0;
        }
    </style>
</head>
<body>
    
    <form action="" method="post">
        <img src="ayam.png">
        <h1>Lagu Anak ayam</h1>
        <label>Banyak anak ayam</label>
        <Input type="number" name="number"></Input>
        <button type="submit" name="mulai">Mulai</button>
    </form>

    <?php if(isset($number)){
        ?>
            <div class='jumlah'>
                <h3>Banyak anak ayam = <?= $number?></h3>
            </div>
        <?php
        for($i = $number; $i >= 1; $i--){
            $sisa = $i - 1;

            if($sisa == 0){
                echo "   
                <div class='lagu'>
                    <p>Anak ayam turun $i <br>Mati satu tinggal Induknya</p>
                </div>";
                exit;
            }
            echo "   
            <div class='lagu'>
                <p>Anak ayam turun $i <br>Mati satu tinggal $sisa</p>
            </div>
            ";
        }
    } ?>
    
</body>
</html>